import React from "react";

type PlayerProps = {
  srcBlob: Blob | null;
  audio: boolean;
};

const Player: React.FC<PlayerProps> = ({ srcBlob = null, audio = true }) => {
  if (!srcBlob) {
    return null;
  }

  if (audio) {
    return <audio src={URL.createObjectURL(srcBlob)} controls />;
  }

  return <video src={URL.createObjectURL(srcBlob)} width={520} height={480} controls />;
};

export default Player;
