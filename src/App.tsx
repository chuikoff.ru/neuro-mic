import React, { useEffect, useMemo, useState } from "react";
import { useInterval } from "ahooks";
import useMediaRecorder from "@wmik/use-media-recorder";
import Player from "./Player";
import { randomItem } from "./helpers/randomItem";

const types = ["анкдот", "историю", "новость", "фантасмагорию"];
const subjects = ["погоду", "технику", "семью", "любимую песню", "известного блогера"];

function App() {
  let { status, mediaBlob, stopRecording, startRecording } = useMediaRecorder({
    onDataAvailable: async (blob) => {
      console.log("blob", blob);
    },
    mediaStreamConstraints: { audio: true },
  });
  const [untilStart, setUntilStart] = useState(5);
  const [timeline, setTimeline] = useState(0);
  const [type] = useState(randomItem(types));
  const [subject] = useState(randomItem(subjects));

  /**
   * Каждую секунду проверяем сколько времени осталось до
   * автоматического старта записи
   */
  useInterval(() => {
    // Если запись не остановлена и время еще есть, уменьшаем каждую секунду
    if (status !== "stopped" && untilStart > 0) {
      setUntilStart((untilStart) => untilStart - 1);
    }
  }, 1000);

  /**
   * Каждую секунду проверяем сколько времени длиться запись
   */
  useInterval(() => {
    // Если запись длиться уже больше 600 секунд, останавливаем автоматически
    if (status === "recording" && timeline >= 600) {
      stopRecording();
    } else if (status === "recording" && timeline < 600) {
      // Если время еще есть, добавляем цифру каждую секунду
      setTimeline((time) => time + 1);
    }
  }, 1000);

  /**
   * Начинаем запись если секунды отсчета кончились и запись еще не начиналась
   */
  useEffect(() => {
    if (untilStart === 0 && status === "idle") {
      startRecording(1000);
    }
  }, [untilStart, startRecording, status]);

  /**
   * Когда запись закончиться, нужно отправить результат на сервер
   */
  useEffect(() => {
    console.log("will send to server", mediaBlob);
  }, [mediaBlob]);

  /**
   * Отображаем таймер записи
   */
  const getTimer = useMemo(() => {
    const time = new Date();
    time.setHours(0, 0, 0, 0);
    time.setSeconds(time.getSeconds() + timeline);
    return `${("0" + time.getMinutes()).slice(-2)}:${("0" + time.getSeconds()).slice(-2)}`;
  }, [timeline]);

  return (
    <div className="neuro-mic">
      <h1 className="neuro-mic__title">
        Расскажи <span className="neuro-mic__title-type">{type}</span> про{" "}
        <span className="neuro-mic__title-subject">{subject}</span>
      </h1>
      {untilStart > 0 ? (
        <p className="neuro-mic__timer">Можете начать через {untilStart} сек.</p>
      ) : (
        <p className="neuro-mic__timer text-red">
          {status === "recording" ? `Идет запись ${getTimer}` : "Ваш спич ушел на анализ!"}
        </p>
      )}
      <button type="button" onClick={() => stopRecording()} disabled={status !== "recording"}>
        Оставить запись
      </button>
      <Player srcBlob={mediaBlob} audio={true} />
    </div>
  );
}

export default App;
