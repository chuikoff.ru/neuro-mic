const express = require("express");
const path = require("path");

const app = express();

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, "build")));

// An api endpoint that returns a pong object
app.get("/ping", (req, res) => {
  res.json({ pong: true });
});

// Handles any requests that don't match the ones above
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/build/index.html"));
});

app.listen(5254);

console.log("App is listening on port 5254");
